void setup() {

  // We still need to call Serial1.begin() -- the baud rate doesn't matter.
  Serial1.begin(250000);

}

// Note on DMX slots: Slots are numbered 1-512 not 0-511.   

void loop() {
  
  for(int x = 0; x < 255; x++)
  {
     // Sets the first DMX channel to x, slow fade from 0-100% and back down in the next loop
     Serial1.set(1,x);
     delay(100); 
  }
  for(int x = 255; x > 0; x--)
  {
     Serial1.set(1,x);
     delay(100); 
  }

}
